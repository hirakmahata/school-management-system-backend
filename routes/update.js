const express = require("express");
const router = express.Router();

const Students = require("../models/Students");


router.put("/update/:id", async(request,response,next) => {

    try{ 
        

        let student_id = request.params.id;
        let student_name = request.body.name;
        let student_age = request.body.age;
        let student_stream = request.body.stream;
        let student_obj={}
        if(student_name !== ""){
            student_obj.student_name =student_name;
        }
        if(student_age !== ""){
            student_obj.student_age = student_age;
        }
        if(student_stream !== ""){
            student_obj.student_stream = student_stream;
        }


        let studentData = await Students.update(student_obj,{
            where:{
                id : student_id
            }
        });
        
        let updatedData = await Students.findAll({
            where:{
                id : student_id
            }
        });

        let name= updatedData[0].student_name;

        


        return response.json({
            message: `you updated ${name} successfully`
        })

 }catch(err){
     next(err)
 }
 });
 
 router.use((err, req, res, next)=>{
    console.error(err.stack);
    res.json({
      message: "internal server error"
    }).end();
  })
 
 module.exports = router;