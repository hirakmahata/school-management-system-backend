const express = require("express");
const router = express.Router();

const Students = require("../models/Students");




router.post("/create", async (request, response, next) => {

    try{
 
         let student_name = request.body.Name;
         let student_age = request.body.age;
         let stream = request.body.stream;
 
 
     if (Name !== ""
         && age !== ""
         && stream !== "") {
            
 
                 let data = await Students.findAll({
                     where:{
                         student_name : student_name
                     }
                 });
                 
                 if (data.length == 0) {
 
                     
 
                     let student={
                         student_name,
                         student_age,
                         stream
                     };
                 
                     let insertedData= await Students.create(student);
 
                     return response.json({
                         message: "inserted successfully"
                     });
 
                        
                 } else {
                     return response.json({
                         message: "student is already there"
                     })
                 }
 
 
     } else {
         return response.json({
             message: "provide every information of the student"
         })
     }
 }catch(err){
         next(err);
     }
 
 });
 
 router.use((err, req, res, next)=>{
     console.error(err.stack);
     res.json({
       message: "internal server error"
     }).end();
   })
 
 
 module.exports = router;