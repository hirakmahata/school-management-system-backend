const express = require("express");
const router = express.Router();

const Students = require("../models/Students");




        router.get("/get/:id", async(request,response,next) => {

            try{ 
                
        
                let student_id = request.params.id;
        
        
                let studentData = await Students.findAll({
                    where:{
                        id : student_id
                    }
                });


                if(studentData.length === 0){
                    return response.json({
                        message: "id does not exist"
                    })
                }else{
                    return response.json({
                        student_details: studentData
                    })
                }
            
        
         }catch(err){
             next(err)
         }
         });
         
         router.use((err, req, res, next)=>{
            console.error(err.stack);
            res.json({
              message: "internal server error"
            }).end();
          })
         
         module.exports = router;

        
