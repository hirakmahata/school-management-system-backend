const express = require("express");
const router = express.Router();

const Students = require("../models/Students");



router.delete("/delete/:id", async(request,response,next) => {

    try{ 
        

        let student_id = request.params.id;


        let studentData = await Students.findAll({
            where:{
                id : student_id
            }
        });
        

        let name= studentData[0].student_name;

        

       let deleted_student = await Students.destroy({
            where:{
                id : student_id
            }
        });


        


        return response.json({
            message: `you deleted ${name} successfully`
        })

 }catch(err){
     next(err)
 }
 });
 
 router.use((err, req, res, next)=>{
    console.error(err.stack);
    res.json({
      message: "internal server error"
    }).end();
  })
 
 module.exports = router;