const env_variable = require('./config.js');
const CORS = require("cors");
const express = require("express");
const sequelize = require("../server/connection");


const Create = require("../routes/create");
const Update = require("../routes/update");
const Delete = require("../routes/delete");
const Get = require("../routes/get");







const app = express();
app.use(express.urlencoded({extended:true}))
app.use(express.json());
app.use(CORS());




app.use(Create);
app.use(Delete);
app.use(Get);
app.use(Update);



app.use((req,res,next)=>{
    res.status(404).json({
      message : "404 not found."
    }).end();
  });



app.use((err, req, res, next)=>{
    console.error(err.stack);
    res.status(500).json({
      message: "internal server error"
    }).end();
  })
 
  sequelize.authenticate()
  .then(()=>{
    sequelize.sync()
  })
  .then(()=>{
    app.listen(env_variable.port,()=>{
      console.log("connection has been established");
      console.log(`server is listening on port: ${env_variable.port}......`);
    })
   })
   .catch(err=>{
     console.error("unable.connect to database",err);
   });