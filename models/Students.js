const Sequelize = require("sequelize");
const sequelize = require("../server/connection")

const Students = sequelize.define("students", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    student_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    student_age: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    stream: {
        type: Sequelize.STRING,
        allowNull: false
    }
    
});

module.exports= Students;